package eu.letsplayonline.analytics.bungee;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

import net.md_5.bungee.api.plugin.Plugin;

/**
 * 
 * @author Julian Fölsch
 */
public abstract class FileInteraction {

	private static Plugin hostPlugin;
	private static boolean fileInteractionError = false;
	public static Properties properties;

	private static final String CONFIG_FOLDER = "plugins/LPOAnalytics/";
	private static final String CONFIG_FILE = CONFIG_FOLDER
			+ "config.properties";

	private static void create() {
		boolean dir = new File(CONFIG_FOLDER).mkdirs();
		File file = new File(CONFIG_FILE);

		if (!dir) {
			fileInteractionError = true;
			hostPlugin.getLogger().log(Level.SEVERE,
					"Error while creating directory!");
		}
		boolean fileCreated = false;
		try {
			fileCreated = file.createNewFile();
		} catch (IOException ioe) {
			fileInteractionError = true;
			hostPlugin.getLogger().log(Level.SEVERE,
					"Error while creating empty file: {0}", ioe);

		}

		if (fileCreated) {
			hostPlugin.getLogger().log(Level.INFO, "Created empty file: {0}",
					file.getPath());

		}
		if (fileCreated) {
			properties = new Properties();
			try {
				properties.setProperty("hostname", "test.example.org");
				properties.setProperty("tracking-id", "UA-xxxxxxxx-x");
				properties.store(new FileOutputStream(CONFIG_FILE), null);
			} catch (IOException ex) {
				hostPlugin.getLogger().log(Level.SEVERE,
						"Error while opening file for writing:", ex);
			}
		}

	}

	public static void startup(Plugin hostPlugin) {
		FileInteraction.hostPlugin = hostPlugin;
		boolean answersExist = new File(CONFIG_FILE).exists();
		if (!answersExist) {
			create();
		}
		if (!fileInteractionError) {
			properties = new Properties();
			BufferedInputStream stream = null;
			try {
				stream = new BufferedInputStream(new FileInputStream(
						CONFIG_FILE));
				properties.load(stream);
			} catch (IOException e) {
				fileInteractionError = true;
				hostPlugin
						.getLogger()
						.log(Level.SEVERE,
								"File config.properties located in plugins/LPONAnalytics not found!",
								e);
			} finally {
				try {
					stream.close();
				} catch (IOException e) {
					// at this point, we don't care
				}
			}

		} else {
			hostPlugin.getLogger().log(Level.WARNING,
					"Looks like there was an error before");
		}
	}
}
