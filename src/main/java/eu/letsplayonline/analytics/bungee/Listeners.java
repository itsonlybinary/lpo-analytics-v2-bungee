package eu.letsplayonline.analytics.bungee;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import eu.letsplayonline.analytics.core.Analytics;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;

/**
 *
 * @author Julian Fölsch
 */
public class Listeners implements Listener {

    private static final Map<UUID, Boolean> activeSessions = new ConcurrentHashMap<>();

    @EventHandler
    public void onServerConnectedEvent(ServerConnectedEvent event) {
        UUID playerUUID = event.getPlayer().getUniqueId();
        String playerVersion = String.valueOf(event.getPlayer().getPendingConnection().getVersion());
        
        if (activeSessions.containsKey(playerUUID) == false) {
            // This is a new session
            activeSessions.put(playerUUID, SQLiInteraction.isOptOut(playerUUID));
            if (activeSessions.get(playerUUID) == false) {
                // Player has not opted out
                Analytics.getInstance().join(
                        EntityFactory.getEntity(event.getPlayer(), event.getServer().getInfo().getName(), playerVersion)
                );
            }
        } else {
            // This is a server change
            if (activeSessions.get(playerUUID) == false) {
                // Player has not opted out
                Analytics.getInstance().changeServer(
                        EntityFactory.getEntity(event.getPlayer(), event.getServer().getInfo().getName(), playerVersion),
                        event.getServer().getInfo().getName()); // TODO why is this needed twice?
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnectEvent(PlayerDisconnectEvent event) {
        UUID playerUUID = event.getPlayer().getUniqueId();
        if (activeSessions.get(playerUUID) == false) {
            // Player has not opted out
            String playerVersion = String.valueOf(event.getPlayer().getPendingConnection().getVersion());
            Analytics.getInstance().leave(
                    EntityFactory.getEntity(event.getPlayer(), "disconnected", playerVersion)
            );
        }
        activeSessions.remove(playerUUID);
    }

    static void setSessionOptOut(UUID playerUUID, boolean value) {
        if (activeSessions.containsKey(playerUUID)) {
            activeSessions.put(playerUUID, value);
        }
    }

    /**
     * Folgende Funktionen des Cores wurden nicht verwendet, da es nicht möglich
     * wäre, sich die benötigten Werte zu besorgen: changeWorld command die kick
     * talk whisper
     */
}
